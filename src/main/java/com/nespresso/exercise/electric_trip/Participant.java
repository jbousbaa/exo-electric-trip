package com.nespresso.exercise.electric_trip;

public class Participant {

	private Car car;
	private String startCity;
	
	public Participant(Car car, String startCity) {
		car = car;
		startCity = startCity;
	}
	
	public int calculateMaxDistance(){
		
		return car.calculateDistanceWithHighSpeedPerformance();
	}
	
	public boolean go(){
		return car.start();
	}
	
	public String startCityName(){
		return startCity;
	}
	
	public String chargeOf(){
		return car.printSize();
	}
	
	
	
}
