package com.nespresso.exercise.electric_trip.printer;

public interface BatteryPrinter {

	public String formattedFormat(double batteryValue);
}
