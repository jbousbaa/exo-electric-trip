package com.nespresso.exercise.electric_trip.printer;

public class BatteryPrinterPoucent implements BatteryPrinter {

	private static final String POURCENT ="%";
	@Override
	public String formattedFormat(double batteryValue) {
		
		return batteryValue+POURCENT;
	}

}
