package com.nespresso.exercise.electric_trip.parser;

import java.util.Map;

public interface PathParser {

	public Map<String,Integer> informationsOfPath(String informations);
}
