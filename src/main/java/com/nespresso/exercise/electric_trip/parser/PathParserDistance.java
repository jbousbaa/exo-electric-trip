package com.nespresso.exercise.electric_trip.parser;

import java.util.HashMap;
import java.util.Map;

public class PathParserDistance implements PathParser {

	private final static String TIRET="-";
	
	@Override
	public Map<String,Integer> informationsOfPath(String informations) {
		
		Map<String,Integer> pathGlobalInformations = new HashMap<String, Integer>();
		String[] pathInformations = informations.split(TIRET);
		for(int i=0 ; i<pathInformations.length;i+=2){
			String cityName = pathInformations[i];
			Integer distance;
			if(i<pathInformations.length-1)
			  distance = Integer.valueOf(pathInformations[i+1]);
			else
			  distance = Integer.valueOf(0);
			pathGlobalInformations.put(cityName, distance);
		}
		return pathGlobalInformations;
	}
	

}
