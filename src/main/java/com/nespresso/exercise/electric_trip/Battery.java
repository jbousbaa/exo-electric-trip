package com.nespresso.exercise.electric_trip;

import com.nespresso.exercise.electric_trip.printer.BatteryPrinter;
import com.nespresso.exercise.electric_trip.printer.BatteryPrinterPoucent;

public class Battery {

	private int size;

	public Battery(int size) {
		size = size;
	}
	
	public String printSize(){
		BatteryPrinter batteryPrinter = new BatteryPrinterPoucent();
		return batteryPrinter.formattedFormat(size);
	}
	
	private String calculateSizeBattery(int distance,int speed){
		    size =  distance/speed;
		    return printSize();
	}
	
	
}
