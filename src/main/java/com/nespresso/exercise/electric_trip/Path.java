package com.nespresso.exercise.electric_trip;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.nespresso.exercise.electric_trip.parser.PathParser;
import com.nespresso.exercise.electric_trip.parser.PathParserDistance;

public class Path {

	private Map<String,Integer> pathInformation;
	
	public Path(String informations){
		PathParser pathParser = new PathParserDistance();
		pathInformation = pathParser.informationsOfPath(informations);
	}
	
	public Map<String, Integer> distanceMustParcoured(Participant participant){
		
		Map<String, Integer> informations = new HashMap<>();
		 Set<String> citiesName = pathInformation.keySet();
		 int distance = 0; 
		 String cityEnd = "";
		 boolean canIContinue = false;
         Iterator<String> iterator = citiesName.iterator();
         while(iterator.hasNext()){
        	 String cityName =iterator.next();
        	 if(cityName==participant.startCityName()){
        		 canIContinue = true;
        	 }
        	 if(canIContinue && distance<participant.calculateMaxDistance()){
        		 distance = pathInformation.get(cityName).intValue(); 
         }
		
	}
        informations.put(cityEnd,distance);
		return informations;
	}
}
	
	
 
