package com.nespresso.exercise.electric_trip;

import com.nespresso.exercise.electric_trip.printer.BatteryPrinter;
import com.nespresso.exercise.electric_trip.printer.BatteryPrinterPoucent;

public class Car {

	private int batterySize;
	private int lowSpeedPerformance;
	private int highSpeedPerformance;
	
	public Car(int batterySize, int lowSpeedPerformance,
			int highSpeedPerformance) {
		batterySize = batterySize;
		lowSpeedPerformance = lowSpeedPerformance;
		highSpeedPerformance = highSpeedPerformance;
	}
	
	public int calculatedistanceWithLowSpeedPerformance(){
		
		return batterySize*lowSpeedPerformance;
	}
	
	public int calculateDistanceWithHighSpeedPerformance(){
		
		return batterySize*highSpeedPerformance;
	}
	
	public boolean start(){
		if(batterySize!=0)
		return true;
		else
	     return false;
	}
	
	public String printSize(){
		BatteryPrinter batteryPrinter = new BatteryPrinterPoucent();
		return batteryPrinter.formattedFormat(batterySize);
	}
	
	private String calculateSizeBattery(int distance,int speed){
		batterySize =  distance/speed;
	    return printSize();
}
	
	
	
}
