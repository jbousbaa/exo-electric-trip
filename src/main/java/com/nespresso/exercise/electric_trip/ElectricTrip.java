package com.nespresso.exercise.electric_trip;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ElectricTrip {

	private Path triptraject;
	private Map<Integer,Participant> participants;
	private static  int PARTICIPANTID = 1;
	
	public ElectricTrip(String informations) {
		triptraject = new Path(informations);
		participants = new HashMap<Integer, Participant>();
	}

	public int startTripIn(String cityName, int batterySize,
			int lowSpeedPerformance, int highSpeedPerformance) {
    
		Car car = new Car(batterySize, lowSpeedPerformance, highSpeedPerformance);
		participants.put(Integer.valueOf(PARTICIPANTID), new Participant(car, cityName));
		return PARTICIPANTID;
	}

	public void go(int participantId) {
		Participant participant = participants.get(Integer.valueOf(participantId));
		participant.go();
		
	}

	public String locationOf(int participantId) {
		Participant participant = participants.get(Integer.valueOf(participantId));  
		if(participant.go() && (participant.calculateMaxDistance() > distanceParcoured(participantId)))
		return cityNameLocation(participantId);
		return null;
	}

	public String chargeOf(int participantId) {
		Participant participant = participants.get(Integer.valueOf(participantId)); 
		
		return participant.chargeOf();
	}

	public void sprint(int participantId) {
		// TODO Auto-generated method stub
		
	}
	
	private String cityNameLocation(int participantId){	
		Participant participant = participants.get(Integer.valueOf(participantId));
		Map<String, Integer> infos = triptraject.distanceMustParcoured(participant);
		
		Set<String> values =  infos.keySet();
		Iterator<String> iterator = values.iterator();
		String nameCity = "";
		while(iterator.hasNext()){
			nameCity =iterator.next();
		}
		return nameCity;
	}
	
	private int distanceParcoured(int participantId){
		Participant participant = participants.get(Integer.valueOf(participantId));
		return triptraject.distanceMustParcoured(participant).get(cityNameLocation(participantId));
	}

	public void charge(int id, int hoursOfCharge) {
		
		
	}

}
